package edu.pte.mik;

import java.util.Random;

public class RandomData {
    private static Random typeRnd = new Random();

    public static int getRandomType(int bound) {
        return typeRnd.nextInt(bound);
    }

    private Random rnd;

    public RandomData(long seed) {
        rnd = new Random(seed);
    }

    public int getInt(int min, int max) { return rnd.nextInt(max - min) + min; }
    public double getDouble(double min, double max) { return min + (max - min) * rnd.nextDouble(); }
    public String getString(String prefix, int min, int max) {
        return String.format("%s%d", prefix, rnd.nextInt(max - min) + min);
    }
}

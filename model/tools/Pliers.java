package edu.pte.mik.model.tools;

public abstract class Pliers extends Tool {
    private int openAngle;

    public int getOpenAngle() {
        return openAngle;
    }

    public Pliers(double length, int electricalInsulation, int openAngle) {
        super(length, electricalInsulation);
        this.openAngle = openAngle;
    }
}

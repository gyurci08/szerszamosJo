package edu.pte.mik.model.tools;

public class PhilipsScrewDriver extends ScrewDriver {
    public PhilipsScrewDriver(int length, int electricalInsulation, int headSize) {
        super(length, electricalInsulation, headSize);
    }

    @Override
    public String toString() {
        return "PhilipsScrewDriver{" +
                "length=" + getLength() +
                ", electricalInsulation=" + getElectricalInsulation() +
                ", headSize=" + getHeadSize() +
                '}';
    }
}

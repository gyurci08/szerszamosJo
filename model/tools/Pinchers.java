package edu.pte.mik.model.tools;

public class Pinchers extends Pliers {
    private int materialWidth;

    public int getMaterialWidth() {
        return materialWidth;
    }

    public Pinchers(double length, int electricalInsulation, int openAngle, int materialWidth) {
        super(length, electricalInsulation, openAngle);
        this.materialWidth = materialWidth;
    }

    @Override
    public void assemble() {
        System.out.println("cut");
    }

    @Override
    public String toString() {
        return "Pin{" +
                "length=" + getLength() +
                ", electricalInsulation=" + getElectricalInsulation() +
                ", openAngle=" + getOpenAngle() +
                "materialWidth=" + materialWidth +
                '}';
    }
}

package edu.pte.mik.model.tools;

public class CombinedPliers extends Pliers {
    private double surface;

    public double getSurface() {
        return surface;
    }

    public CombinedPliers(double length, int electricalInsulation, int openAngle, double surface) {
        super(length, electricalInsulation, openAngle);
        this.surface = surface;
    }

    @Override
    public void assemble() {
        System.out.println("press/hold/cut");
    }

    @Override
    public String toString() {
        return "CombinedPliers{" +
                "length=" + getLength() +
                ", electricalInsulation=" + getElectricalInsulation() +
                ", openAngle=" + getOpenAngle() +
                "surface=" + surface +
                '}';
    }
}

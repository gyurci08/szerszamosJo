package edu.pte.mik.model.tools;

public abstract class Tool implements Comparable<Tool> {
    private double length;
    private int electricalInsulation;

    public double getLength() {
        return length;
    }

    public int getElectricalInsulation() {
        return electricalInsulation;
    }

    abstract public void assemble();

    public Tool(double length, int electricalInsulation) {
        this.length = length;
        this.electricalInsulation = electricalInsulation;
    }

    public int compareTo(Tool other) {
        if(other.getElectricalInsulation() == this.getElectricalInsulation()) {
            return Double.compare(this.length, other.getLength());
        }
        else {
            return Integer.compare(this.getElectricalInsulation(), other.getElectricalInsulation());
        }
    }
}

package edu.pte.mik.model.tools;

public class FlatScrewDriver extends ScrewDriver {
    public FlatScrewDriver(int length, int electricalInsulation, int headSize) {
        super(length, electricalInsulation, headSize);
    }

    @Override
    public String toString() {
        return "FlatScrewDriver{" +
                "length=" + getLength() +
                ", electricalInsulation=" + getElectricalInsulation() +
                ", headSize=" + getHeadSize() +
                '}';
    }
}

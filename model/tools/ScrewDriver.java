package edu.pte.mik.model.tools;

public abstract class ScrewDriver extends Tool {
    private double headSize;

    public double getHeadSize() {
        return headSize;
    }

    public ScrewDriver(int length, int electricalInsulation, int headSize) {
        super(length, electricalInsulation);
        this.headSize = headSize;
    }

    @Override
    public void assemble() {
        System.out.println("turn/fix screw head");
    }
}

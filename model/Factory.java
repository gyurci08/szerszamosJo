package edu.pte.mik.model;

import edu.pte.mik.RandomData;
import edu.pte.mik.model.tools.*;

public class Factory {
    //Limits were not specified in the task,
    //so if no limits are set, it is also acceptable
    //Except electrical insulation
    private static final int LENGTH_MIN = 5;
    private static final int LENGTH_MAX = 30;
    private static final int INSULATION_MIN = 1;
    private static final int INSULATION_MAX = 10;
    private static final int HEAD_MIN = 1;
    private static final int HEAD_MAX = 10;
    private static final int ANGLE_MIN = 20;
    private static final int ANGLE_MAX = 90;
    private static final double SURFACE_MIN = 0.7;
    private static final double SURFACE_MAX = 2;
    private static final int WIDTH_MIN = 1;
    private static final int WIDTH_MAX = 12;

    public static Tool createRandomItems(RandomData[] generator) {
        int type = RandomData.getRandomType(4);
        switch (type) {
            case 0:
                return new FlatScrewDriver(
                        generator[type].getInt(LENGTH_MIN, LENGTH_MAX),
                        generator[type].getInt(INSULATION_MIN, INSULATION_MAX),
                        generator[type].getInt(HEAD_MIN, HEAD_MAX)
                );

            case 1:
                return new PhilipsScrewDriver(
                        generator[type].getInt(LENGTH_MIN, LENGTH_MAX),
                        generator[type].getInt(INSULATION_MIN, INSULATION_MAX),
                        generator[type].getInt(HEAD_MIN, HEAD_MAX)
                );

            case 2:
                return new CombinedPliers(
                        generator[type].getDouble(LENGTH_MIN, LENGTH_MAX),
                        generator[type].getInt(INSULATION_MIN, INSULATION_MAX),
                        generator[type].getInt(ANGLE_MIN, ANGLE_MAX),
                        generator[type].getDouble(SURFACE_MIN, SURFACE_MAX)
                );

            default:
                return new Pinchers(
                        generator[type].getInt(LENGTH_MIN, LENGTH_MAX),
                        generator[type].getInt(INSULATION_MIN, INSULATION_MAX),
                        generator[type].getInt(ANGLE_MIN, ANGLE_MAX),
                        generator[type].getInt(WIDTH_MIN, WIDTH_MAX)
                );

        }
    }
}

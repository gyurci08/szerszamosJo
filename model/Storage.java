package edu.pte.mik.model;

import edu.pte.mik.model.tools.Tool;

import java.util.ArrayList;
import java.util.Collections;

public class Storage {
    private ArrayList<Tool> tools = new ArrayList<>();

    public void add(Tool newItem) {
        tools.add(newItem);
    }

    public int getSize() { return tools.size(); }

    public Tool get(int index) { return tools.get(index); }

    public void sort() {
        Collections.sort(tools);
    }
}

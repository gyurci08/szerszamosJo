package edu.pte.mik;

import edu.pte.mik.model.Factory;
import edu.pte.mik.model.Storage;
import edu.pte.mik.model.tools.Tool;

public class Controller {
    private static final int[] SEEDS = {3000, 6000, 9000, 12000};
    private static int ITEM_COUNT = 25;

    public void main() {
        Storage storage = new Storage();
        RandomData[] generators = new RandomData[4];

        for(int i = 0; i < SEEDS.length; i++) {
            generators[i] = new RandomData(SEEDS[i]);
        }

        for(int i = 0; i < ITEM_COUNT; i++) {
            storage.add(Factory.createRandomItems(generators));
        }

        storage.sort();

        Tool currentTool;
        for(int i = 0; i < storage.getSize(); i++) {
            currentTool = storage.get(i);
            System.out.println(currentTool);
            currentTool.assemble();
        }
    }
}
